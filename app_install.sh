#!/bin/bash/

echo "Update repo..."
sudo apt update

echo "Upgrade repo..."
sudo apt upgrade

echo "install Tor and Tor socks..."
sudo apt install tor 
apt install torsocks

echo "install git"
sudo apt install git

echo "Install Postgresql and Postgresql contrib ..."

sudo apt-get install -y -qq postgresql postgresql-contrib libpq-dev 
sudo /bin/systemctl enable postgresql
sudo /bin/systemctl start postgresql
service_isactive "postgresql"

echo "Install redis server..."
sudo apt-get install -y -qq redis-server
sudo /bin/systemctl enable redis-server
sudo /bin/systemctl start redis-server
service_isactive "redis-server"

echo "Create gitconfig and set the settings" 
touch ~/.gitconfig 
read -p 'gitlab email: ' emailvar
read -p 'Username: ' uservar

cat << EOF >> ~/.gitconfig
[user]
	email = $emailvar
	name = $uservar
[http]
	proxy = socks5://localhost:9050

EOF

echo "Create Core folder and clone it ..."
mkdir core_project
cd core_project
read -p 'Please Enter the SSH Clone URL: ' urlvar
torsocks git clone $urlvar
cd ~

echo "Install curl distutil pip virtualenv ..."
sudo apt install curl
sudo apt-get install python3-distutils
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo -H python3 get-pip.py
sudo -H pip3 install virtualenv

echo "Setup bashrc ...."
cat << EOF >> ~/.bashrc
alias runenv='source core/bin/activate'
alias runcoreproject='python manage.py runserver --settings=Security_advisory_core.settings.dev'
alias gotoproject='cd core_project/Security_advisory_core'
EOF

echo "Create virtualenv...." 
virtualenv core
sudo chmod +x core_project/Security_advisory_core/manage.py
sudo chmod +777 core_project/Security_advisory_core/db/init_db.sql
